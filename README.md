# Gaping - An HTTP API to Genesys

Gaping acts as an HTTP gateway to backend Genesys servers. It offers connections to the servers through URLs, and translates Genesys protocol messages from/to JSON.

It is currently in a very preliminary state.

## Configuration

In your Genesys Configuration, create an Application of type "Genesys Generic Server". We suggest the name "API_Server", but it can be any name you want.

Assign the Application a default port. That is the port that the HTTP service will run on, for example 8080.

Configure Connections to the backend Genesys servers that you want to get access to through the HTTP API.

## Running the Server

This project is written in [Clojure](https://clojure.org/), using the [Leiningen](https://leiningen.org/) build manager.

Install Leiningen.

Clone this project.

Install the Genesys Platform SDK libraries in your Maven repository, as described [here](https://developer.genesys.com/2017/02/22/platform-sdk-as-maven-dependency/).

Edit the `project.clj` file. Update `:jvm-opts`for setting the correct parameters to access your Genesys Configuration. 

From the project's root folder, run from the console:

    lein run

## HTTP Endpoints

### POST /connection/(application_name)/request

Sends a request to the server, waits for the server response, and receives the response in a 200 OK.

### POST /connection/\(application_name)/send

Sends a request to the server and receives an empty response 204 No Content.

### Bayeux (CometD) endpoint: /bayeux

Server events are broadcasted through Bayeux channels. Use a Bayeux client to receive those.

## Usage Samples

Let's say you have configured a connection to a Stat Server with application name "Stat_Server". You could do this to subscribe to a statistic:

    POST /connection/Stat_Server/request
    {
      "messageName": "com.genesyslab.platform.reporting.protocol.statserver.requests.RequestOpenStatistic",
      "messageData": {
        "statisticObject": {
          "objectId": "KSippola",
          "objectType": "Agent",
          "tenantName": "Environment",
          "tenantPassword": ""
        },
        "statisticMetric": {
          "statisticType": "Total_Calls"
        },
        "notification": {
          "mode": "Periodical",
          "frequency": "15"
        },
        "tag": 35
      }
    }

You will receive the following response:

    HTTP 200 OK
    {
      "messageName": "EventStatisticOpened",
      "messageData": {
        "intervalLength": 0,
        "referenceId": 1,
        "tag": 35,
        "timestamp": 1489085059,
        "intValue": 0
      }
    }

Another example with a request to a Genesys SIP Server:

    POST /connection/SIP_Server/request
    {
      "messageName": "com.genesyslab.platform.voice.protocol.tserver.requests.dn.RequestRegisterAddress",
      "messageData": {
        "registerMode": "ModeShare",
        "controlMode": "RegisterDefault",
        "addressType": "DN",
        "thisDN": "7001"
      }
    }

If everything goes well, you will receive a response:

    HTTP 200 OK
    {
      "messageName": "EventRegistered",
      "messageData": {
        "addressInfoType": "AddressType",
        "addressInfoStatus": 1,
        "referenceID": 1,
        "thisDN": "7001",
        "customerID": "Environment",
        "extensions": [
          {
            "key": "AgentStatus",
            "type": "int",
            "value": -1
          },
          {
            "key": "AgentStatusTimestamp",
            "type": "int",
            "value": 0
          },
          {
            "key": "status",
            "type": "int",
            "value": 0
          }
        ],
        "addressType": "DN",
        "eventSequenceNumber": 1241,
        "time": {
          "timeinSecs": 1489087971,
          "timeinuSecs": 698000
        }
      }
    }
   
## Sample Page for receiving Bayeux messages

You can open the file `/client/index.html` in this project with your browser, in order to receive events easily.

But first modify the file to make sure that the code contains the right URL to your server.