(defproject gaping "0.1.0-SNAPSHOT"
  :description "Just APIs for Genesys"
  :url "http://example.com/FIXME"
  :dependencies
  [
   [org.clojure/clojure "1.8.0"]
   [functionalbytes/mount-lite "2.0.0-SNAPSHOT"]
   [ring/ring-core "1.5.0"]
   [ring/ring-jetty-adapter "1.5.0"]
   [bidi "1.20.0"]
   [org.eclipse.jetty/jetty-servlet "9.2.10.v20150310"]
   [org.cometd.java/cometd-java-server "3.0.4"]
   [org.clojure/data.json "0.2.6"]
   [ring-logger "0.7.7"]
   [org.slf4j/slf4j-jdk14 "1.7.23"]
   [com.genesyslab.platform/jackson2-module "853.2.2"]
   [com.genesyslab.platform/warmstandbyappblock "853.2.2"]
   [com.genesyslab.platform/configurationprotocol "853.2.2"]
   [com.genesyslab.platform/contactsprotocol "853.2.2"]
   [com.genesyslab.platform/managementprotocol "853.2.2"]
   [com.genesyslab.platform/openmediaprotocol "853.2.2"]
   [com.genesyslab.platform/outboundprotocol "853.2.2"]
   [com.genesyslab.platform/reportingprotocol "853.2.2"]
   [com.genesyslab.platform/routingprotocol "853.2.2"]
   [com.genesyslab.platform/voiceprotocol "853.2.2"]
   [com.genesyslab.platform/webmediaprotocol "853.2.2"]
  ]
  :main ^:skip-aot gaping.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}
  :jvm-opts [
             "-Djava.util.logging.config.file=logging.properties"
             "-Dcom.genesyslab.platform.commons.log.loggerFactory=slf4j"
             "-Dgaping.applicationName=API_Server"
             "-Dgaping.configServer.url=tcp://demosrv:2020"
             "-Dgaping.configServer.username=demo"
             "-Dgaping.configServer.password="
            ]
)
