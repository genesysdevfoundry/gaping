(ns gaping.options)

(defn option [options key]
  (System/getProperty (str "gaping." (name key))))

; options implement ILookup so that you can do (:key options)
(def options
  (reify clojure.lang.ILookup
    (valAt [this key]
      (option this key))
    (valAt [this key not-found]
      (let [res (option this key)]
        (if (nil? res) not-found res)))))

