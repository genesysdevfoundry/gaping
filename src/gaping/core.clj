(ns gaping.core
  (:gen-class)
  (:require [gaping.configuration :as configuration]
            [gaping.server :as server]))

(defn start []
  (configuration/start))

; incomplete stop
(defn stop [system]
  (configuration/stop (:config system))
  (gaping.server/stop @(:server-ref system)))

(defn -main [& args]
  (let [sys (configuration/start)]))
