(ns gaping.configuration
  (:require [gaping.options :refer [options]]
            [gaping.connections :as connections]
            [gaping.server :as server]
            [clojure.tools.logging :as log])
  (:import [com.genesyslab.platform.configuration.protocol.types
            CfgObjectType CfgAppType]))

; Utility functions for reading configuration objects 
(defn read-objs [config-server obj-type init-filter]
  (.request
    config-server
    ; We are just processing the first response, which should be an EventObjectsRead.
    ; There's an additional EventObjectsSent event that we are not processing
    (doto (com.genesyslab.platform.configuration.protocol.confserver.requests.objects.RequestReadObjects/create)
      (.setObjectType (.ordinal obj-type))
      (.setFilter (doto (com.genesyslab.platform.commons.collections.KeyValueCollection.)
                    init-filter)))))

(defn read-obj [config-server obj-type init-filter]
  (let [resp (read-objs config-server obj-type init-filter)]
    (assert (= 1 (.getObjectCount resp)))
    (-> resp .getObjects first)))

(defn read-obj-by-dbid [config-server obj-type dbid]
  (read-obj config-server obj-type #(.addInt % "dbid" dbid)))

(defn register-notification [config-server obj-type tenant-id dbid]
  (.request
    config-server
    (doto (com.genesyslab.platform.configuration.protocol.confserver.requests.objects.RequestRegisterNotification/create)
      (.setSubscription
        (doto (com.genesyslab.platform.commons.collections.KeyValueCollection.)
          (.addList "1" (doto (com.genesyslab.platform.commons.collections.KeyValueCollection.)
            (.addInt "object_type" (.ordinal obj-type))
            (.addInt "tenant_id" tenant-id)
            (.addInt "object_dbid" dbid))))))))

; Functions for creating connections from application configuration
(def app-type->protocol-ctor
  {
    (.ordinal CfgAppType/CFGChatServer)
    #(com.genesyslab.platform.webmedia.protocol.FlexChatProtocol.)

    (.ordinal CfgAppType/CFGCMServer)
    #(com.genesyslab.platform.outbound.protocol.OutboundServerProtocol.)

    (.ordinal CfgAppType/CFGContactServer)
    #(com.genesyslab.platform.contacts.protocol.UniversalContactServerProtocol.)

    (.ordinal CfgAppType/CFGEmailServer)
    #(com.genesyslab.platform.webmedia.protocol.EmailProtocol.)

    (.ordinal CfgAppType/CFGInteractionServer)
    #(com.genesyslab.platform.openmedia.protocol.InteractionServerProtocol.)

    (.ordinal CfgAppType/CFGMessageServer)
    #(com.genesyslab.platform.management.protocol.MessageServerProtocol.)

    (.ordinal CfgAppType/CFGRouterServer)
    #(com.genesyslab.platform.routing.protocol.RoutingServerProtocol.)

    (.ordinal CfgAppType/CFGSCE)
    #(com.genesyslab.platform.configuration.protocol.ConfServerProtocol.)

    (.ordinal CfgAppType/CFGSCS)
    #(com.genesyslab.platform.management.protocol.SolutionControlServerProtocol.)

    (.ordinal CfgAppType/CFGStatServer)
    #(com.genesyslab.platform.reporting.protocol.StatServerProtocol.)

    (.ordinal CfgAppType/CFGTServer)
    #(com.genesyslab.platform.voice.protocol.TServerProtocol.)
  })

(defn create-endpoint [config-server endpoint-name server-info]
  (let [host (read-obj-by-dbid config-server
                               CfgObjectType/CFGHost
                               (.getPropertyValue server-info "hostDBID"))
        hostname (.getPropertyValue host "name")
        port (Integer/parseInt (.getPropertyValue server-info "port"))]
    (com.genesyslab.platform.commons.protocol.Endpoint. endpoint-name hostname port)))

(defn create-backup-endpoint
  "Returns nil if no backup configured"
  [config-server primary-endpoint-name primary-server-info]
  (let [backup-server-dbid (.getPropertyValue primary-server-info "backupServerDBID")]
    (when (not= backup-server-dbid 0)
      (let [backup-endpoint-name (str primary-endpoint-name "#backup")
            backup-server (read-obj config-server CfgObjectType/CFGApplication backup-server-dbid)
            backup-server-info (.getPropertyValue backup-server "serverInfo")]
      (create-endpoint config-server backup-endpoint-name backup-server-info)))))

(defn server->protocol [config-server server]
  (let [server-name (.getPropertyValue server "name")
        ctor (app-type->protocol-ctor (.getPropertyValue server "type"))
        server-info (.getPropertyValue server "serverInfo")
        primary-endpoint (create-endpoint config-server server-name server-info)
        endpoints (if-let [backup-endpoint (create-backup-endpoint config-server server-name server-info)]
                    [primary-endpoint backup-endpoint]
                    [primary-endpoint])
        gchannel (doto (ctor)
                   (.setClientName (:applicationName options))
                 (.setMessageHandler
                   (reify com.genesyslab.platform.commons.protocol.MessageHandler
                       (onMessage [this msg]
                         (connections/publish-event server-name msg)))))
        standby (com.genesyslab.platform.standby.WarmStandby.
                  (str server-name "-WarmStandy")
                  gchannel
                  endpoints)]
    [server-name (connections/->PsdkGenesysConnection gchannel standby)]))

(defn add-connections [config-server cfg-app]
  (doseq [conn-info (.getPropertyValue cfg-app "appServerDBIDs")]
    (let [server-dbid (.getPropertyValue conn-info "appServerDBID")
          server-app (read-obj-by-dbid config-server CfgObjectType/CFGApplication server-dbid) 
          [server-name channel] (server->protocol config-server server-app)]
      (connections/add-connection server-name server-dbid channel)))

(defn handle-app-updated [msg config-server cfg-app]
  (when (and (instance? com.genesyslab.platform.configuration.protocol.confserver.events.EventObjectUpdated msg)
             (= (.getObjectType msg) (.ordinal CfgObjectType/CFGApplication)))
    (let [delta (.getObjectDelta msg)]
      (when (= (.getObjectDbid delta) (.getPropertyValue cfg-app "DBID"))
        (if-let [delta-app (.getPropertyValue delta "deltaApplication")]
          (add-connections config-server delta-app)))
        (if-let [deleted (.getPropertyValue delta "deletedAppServerDBIDs")]
          (doseq [dbid deleted]
            (connections/remove-connection dbid)))))))

(defn read-app [config-server]
  (read-obj
    config-server
    CfgObjectType/CFGApplication
    #(.addString % "name" (:applicationName options))))

(defn start-server [config-server config-event-handlers]
  (let [cfg-app (read-app config-server)
        port (-> cfg-app
               (.getPropertyValue "serverInfo")
               (.getPropertyValue "port")
               (Integer/parseInt))]
 (add-connections config-server cfg-app)
 (swap! config-event-handlers conj #(handle-app-updated % config-server cfg-app))
 (register-notification
      config-server
      CfgObjectType/CFGApplication
      0
      (.getPropertyValue cfg-app "DBID"))
    (server/start port)))

(defn new-config-server []
  (doto
    (com.genesyslab.platform.configuration.protocol.ConfServerProtocol.
      (com.genesyslab.platform.commons.protocol.Endpoint.
        (java.net.URI. (:configServer.url options))))
    (.setClientApplicationType (.ordinal CfgAppType/CFGGenericServer))
    (.setClientName (:applicationName options))
    (.setUserName (:configServer.username options))
    (.setUserPassword (:configServer.password options))))

(defn start []
  (let [config-server (new-config-server)
        server-promise (promise)
        config-event-handlers (atom [])]
    (.setMessageHandler config-server
      (reify com.genesyslab.platform.commons.protocol.MessageHandler
        (onMessage [this msg]
          (doseq [h @config-event-handlers] (h msg)))))
    (.addChannelListener config-server
      (reify com.genesyslab.platform.commons.protocol.ChannelListener
        (onChannelOpened [this ev]
          (deliver server-promise (start-server config-server config-event-handlers)))
        (onChannelClosed [this ev] nil)
        (onChannelError [this ev] nil)))
    (.open config-server)
    {:config config-server
     :server-ref server-promise}))

(defn stop [config-server]
  (.close config-server))