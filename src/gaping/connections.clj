(ns gaping.connections)

; defining Genesys connections as a protocol.
; May be not the best way to go, but let's find out why.
; If it is not really appropriate, revert to plain functions.
(defprotocol GenesysConnection
  (ref-builder [this])
  (set-message-handler [this handler])
  (send-message [this msg])
  (request-message [this msg])
  (open [this])
  (close [this]))

(deftype PsdkGenesysConnection [channel standby]
  GenesysConnection
  (ref-builder [this]
    (.getReferenceBuilder channel))
  (set-message-handler [this handler]
    (.setMessageHandler channel handler))
  (send-message [this msg]
    (.send channel msg))
  (request-message [this msg]
    (.request channel msg))
  (open [this]
    (.autoRestore standby))
  (close [this]
    (.closeAsync standby)))

; redefine as single agent with a map. use pattern matching to define add and remove methods
(def name->connection (atom {}))
(def dbid->name (atom {}))
(def event-handlers (atom []))

(defn add-connection [name dbid connection]
  (swap! name->connection assoc name connection)
  (swap! dbid->name assoc dbid name)
  (open connection))

(defn remove-connection [dbid]
 (let [name (@dbid->name dbid)
       connection (@name->connection name)]
   (swap! name->connection dissoc name)
   (swap! dbid->name dissoc dbid)
   (close connection)))

(defn get-connection [name]
  (@name->connection name))

(defn add-event-handler [h]
  (swap! event-handlers conj h))

(defn remove-event-handler [h]
  (swap! event-handlers remove h))

(defn publish-event [server-name msg]
  (doseq [handler @event-handlers]
    (handler server-name msg)))