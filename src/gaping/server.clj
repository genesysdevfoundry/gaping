(ns gaping.server
  (:require
    [gaping.connections :as connections]
    [bidi.ring]
    [ring.adapter.jetty]
    [clojure.data.json]
    [clojure.java.io]
    [clojure.tools.logging :as log]
    [ring.logger]))

(def psdk-jackson-databind-module
  (com.genesyslab.platform.json.jackson2.PSDKModule.
;    (-> configuration/config-server .connectionContext .serverContext .getMetadata)
    ))

(def json-mapper
  (doto (com.fasterxml.jackson.databind.ObjectMapper.)
    (.registerModule psdk-jackson-databind-module)))

(defn build-message [req]
  (let [req-tree (.readTree json-mapper (:body req))]
    (.treeToValue json-mapper
      (.get req-tree "messageData")
      (Class/forName (-> req-tree (.get "messageName") .asText)))))

(defn connection-send [req]
  (let [gchannel (connections/get-connection (-> req :params :connection-name))]
    (connections/send-message gchannel (build-message req))
    {:status 204})) ; 204 No Content

(defn connection-request [req]
  (let [message (build-message req)
        gchannel (connections/get-connection (-> req :params :connection-name))
        response (connections/request-message gchannel message)]
    {
      :status 200 ; 200 OK
      :headers {"Content-Type" "application/json"}
      :body (.writeValueAsString json-mapper
      {
        "messageName" (.messageName response),
        "messageData" response
      })
    }))

(def request-handler
  (bidi.ring/make-handler
    ["/" {["connection/" :connection-name "/send"] {:post connection-send}
          ["connection/" :connection-name "/request"] {:post connection-request}}]))

(def handler
  (-> request-handler
      ring.logger/wrap-with-body-logger
      ring.logger/wrap-with-logger
      ))

(defn on-bayeux-server-created [bayeux-server]
  ;https://docs.cometd.org/current/reference/#_java_json_customization
  (-> bayeux-server
    (.getOption "jsonContext")
    .getObjectMapper
    (.registerModule psdk-jackson-databind-module))
  (let [bayeux-local-session (.newLocalSession bayeux-server "events")]
    (.handshake bayeux-local-session)
    (connections/add-event-handler
      (fn [server-name msg]
        (-> bayeux-local-session
          (.getChannel (str "/" server-name))
          (.publish
            {
             "messageName" (.messageName msg),
             "messageData" msg
            }))))))

(defn add-bayeux-handler [parent context-path servlet-path]
  ; see https://docs.cometd.org/current/reference/#_installation_jetty_embedded
  (doto (org.eclipse.jetty.servlet.ServletContextHandler. parent context-path)
    (.addServlet
      (doto (org.eclipse.jetty.servlet.ServletHolder. org.cometd.server.CometDServlet)
        (.setAsyncSupported true)
        ;https://docs.cometd.org/current/reference/#_java_json_server_config
        (.setInitParameter "jsonContext" "org.cometd.server.Jackson2JSONContextServer"))
      servlet-path)
    ; see https://docs.cometd.org/current/reference/#_integration_via_configuration_listener
    (.addEventListener
      (reify javax.servlet.ServletContextAttributeListener
        (attributeAdded [this event]
          (when (= org.cometd.bayeux.server.BayeuxServer/ATTRIBUTE (.getName event))
            (on-bayeux-server-created (.getValue event))))
        (attributeRemoved [this event] nil)
        (attributeReplaced [this event] nil)))))

(defn setup-bayeux [server context-path servlet-path]
  (let [parent-handler (org.eclipse.jetty.server.handler.HandlerList.)
        existing-handler (.getHandler server)]
    (add-bayeux-handler parent-handler context-path servlet-path)
    (.addHandler parent-handler existing-handler)
    (.setHandler server parent-handler)))

(defn start [port]
  (ring.adapter.jetty/run-jetty
    #'handler
    {:port port
     :join? false
     :configurator #(setup-bayeux % "/bayeux" "/*")}))

(defn stop [server]
  (.stop server))